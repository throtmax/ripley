@Library('scriptsDimensions') _

pipeline {
    agent {label 'master'}
    stages {
        stage("Initialize Pipeline") {
            steps {
                script {     
                    def props = readProperties file: 'JenkinsParams'

                    env.REGISTRY_URL = props['REGISTRY_URL']
                    // env.REGISTRY_NAME = props['REGISTRY_NAME']
                    env.SLACK_CHANNEL = props['SLACK_CHANNEL']
                    // env.EKS_CLUSTER_DEV = props['EKS_CLUSTER_DEV']
                    // env.EKS_CLUSTER_PP = props['EKS_CLUSTER_PP']
                    // env.EKS_CLUSTER_PROD = props['EKS_CLUSTER_PROD']
                    env.PROJECT_NAME = props['PROJECT_NAME']

                    def ver_file = readMavenPom file: 'src/pom.xml'
                    env.VERSION = ver_file.version

                    env.SHORT_COMMIT = env.GIT_COMMIT.substring(0,8)
                    env.GIT_REPO_URL = env.GIT_URL_1 ?: env.GIT_URL 
                    env.GIT_REPO_NAME = env.GIT_REPO_URL.replaceFirst(/^.*\/([^\/]+?).git$/, '$1')
                    env.BRANCH_NAME = env.BRANCH_NAME.replaceAll('\\/','_')

                    if ( env.BRANCH_NAME && env.BRANCH_NAME ==~ /(development|release)/) { 
                        env.TAG_NAME = env.BRANCH_NAME
                    }
                    if ( env.CHANGE_TARGET && env.CHANGE_TARGET == 'release') { 
                        env.TAG_NAME = env.CHANGE_TARGET
                    }


                    //Para Despliegue en Dimensions
                    env.PROYECTO = "AdmAntecedentesServicesEAR"   									                        	//Para identificar las configuraciones
                    env.NOMBREBINARIO = "AdmAntecedentesServicesEAR-1.0-SNAPSHOT.ear"  	                                        //Nombre del binario
                    env.RUTABINARIO = "/root/.m2/repository/cl/ripley/banco/adm/AdmAntecedentesServicesEAR/1.0-SNAPSHOT/"		//Ruta del binario

                    //Enviar Notificacion a Slack
                    slackSend(channel: "#${env.SLACK_CHANNEL}", color:"warning", message: "PIPELINE - BUILDING -> REPO: ${env.GIT_REPO_NAME} - BRANCH_NAME: ${env.BRANCH_NAME} - INITIALIZING..." )
                }
            }
        } 

        // stage('Build app') {
        //     when {
        //         anyOf {
        //             expression { env.BRANCH_NAME != null && env.BRANCH_NAME ==~ /(feature|bugfix|development).*/ }
        //             expression { env.CHANGE_TARGET != null && CHANGE_TARGET ==~ /(development|release)/ }
        //         }
        //     }
        //     steps {
        //         script {
        //             stash name: "source_code"
        //             sh 'chmod +x mvnw'
        //             // sh './mvnw clean install -DskipTests'
        //             sh './mvnw clean install -f src/pom.xml -DskipTests'
        //         }
        //     }
        // }
        stage('Build app') {
            when {
                anyOf {
                    expression { env.BRANCH_NAME != null && env.BRANCH_NAME ==~ /(feature|bugfix|development).*/ }
                    expression { env.CHANGE_TARGET != null && CHANGE_TARGET ==~ /(development|release)/ }
                }
            }
            steps {
                script {
                    stash name: "source_code"

                    sh 'chmod +x mvnw'
                    sh './mvnw clean install -f src/pom.xml -DskipTests -am'
                    // sh 'mvn clean install -f src/pom.xml -DskipTests'
                }
            }
        }
     
        // stage('Test') {
        //     // when { 
        //     //     anyOf {
        //     //         expression { env.BRANCH_NAME != null && env.BRANCH_NAME ==~ /(feature|bugfix|development).*/ }
        //     //         expression { env.CHANGE_TARGET != null && CHANGE_TARGET ==~ /(development|release)/ }
        //     //     }
        //     //     beforeAgent true
        //     // }
        //     steps {
        //         echo 'Ejecutando pruebas'
        //         sh 'chmod +x mvnw'
        //         sh './mvnw test -f src/pom.xml'
        //     }
        // } 
            
        // stage('Build Docker Image') {
        //     // when {
        //     //     anyOf {
        //     //        expression { env.BRANCH_NAME != null && env.BRANCH_NAME ==~ /(development)/ }
        //     //        expression { env.CHANGE_TARGET != null && CHANGE_TARGET ==~ /(release)/ }
        //     //     }
        //     // }
        //     steps{
        //         script {
        //             docker.build("${env.REGISTRY_URL}/${env.REGISTRY_NAME}:build_${env.GIT_REPO_NAME}_B${env.BUILD_NUMBER}_C${env.SHORT_COMMIT}")
        //             sh "docker tag ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:build_${env.GIT_REPO_NAME}_B${env.BUILD_NUMBER}_C${env.SHORT_COMMIT} ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:build_${env.GIT_REPO_NAME}_${env.TAG_NAME}_${env.VERSION}"

        //             docker.withRegistry( "https://${env.REGISTRY_URL}", 'ecr:us-east-1:aws-credential-registry') {
        //                 sh "docker push ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:build_${env.GIT_REPO_NAME}_${env.TAG_NAME}_${env.VERSION}"
        //             }
                
        //         }
        //     }
        // }


        stage('Deploy to Dimensions') {
        	when { expression { env.BRANCH_NAME != null && BRANCH_NAME ==~ /(development)/ }}
        	steps{
        		script{
					//Despliegue
        			desplegar()
        			
					//Enviar Notificacion a Slack
        			//slackSend(channel: "#${env.SLACK_CHANNEL}", message: "PIPELINE - DEPLOYING  -  DIMENSIONS ${env.BRANCH_NAME} - VERSION: ${env.TAG_NAME}_B${env.BUILD_NUMBER}_C${env.SHORT_COMMIT}" )
        		}
        	}
        }


        // stage('DEPLOY'){
        //     when { expression { env.BRANCH_NAME ==~ /(development|release|master)/ }}
        //     parallel{
        //         stage('Deploy to Development') {
        //             when { expression { env.BRANCH_NAME ==~ /(development)/ }}
        //             steps{
        //                 script{
        //                     sh "docker tag ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:build_${env.GIT_REPO_NAME}_B${env.BUILD_NUMBER}_C${env.SHORT_COMMIT} ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:${env.GIT_REPO_NAME}_dev_B${env.BUILD_NUMBER}_C${env.SHORT_COMMIT}"
                            
        //                     docker.withRegistry( "https://${env.REGISTRY_URL}", 'ecr:us-east-1:aws-credential-registry') {
        //                         sh "docker push ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:${env.GIT_REPO_NAME}_dev_B${env.BUILD_NUMBER}_C${env.SHORT_COMMIT}"
        //                     }

        //                     //Enviar Notificacion a Slack
        //                     slackSend(channel: "#${env.SLACK_CHANNEL}", color:"warning", message: "PIPELINE - DEPLOYING -> ${env.GIT_REPO_NAME} -> ${env.EKS_CLUSTER_DEV} - VERSION: ${env.BUILD_NUMBER}_C${env.SHORT_COMMIT}" )
        //                     slackSend(channel: "#${env.SLACK_CHANNEL}", color:"good", message: "PIPELINE - WAITING FOR SPINNAKER..." )
        //                 }
        //             }
        //         }

        //         stage('Deploy to PP') {
        //             when { expression { env.BRANCH_NAME ==~ /(release)/ }}
        //             steps{
        //                 script {

        //                     echo "Deploying version... ${env.GIT_REPO_NAME}_${env.TAG_NAME}_${env.VERSION}"

        //                     sh 'git config --local credential.helper "!p() { echo username=\\$GIT_USERNAME; echo password=\\$GIT_PASSWORD; }; p"'
        //                     sh "git tag ${env.VERSION}"
        //                     withCredentials([usernamePassword(credentialsId: 'bitbucket', usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD')]) {
        //                         sh "git push origin : ${env.VERSION}"
        //                     }

        //                     docker.withRegistry( "https://${env.REGISTRY_URL}", 'ecr:us-east-1:aws-credential-registry') {
        //                         sh "docker pull ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:build_${env.GIT_REPO_NAME}_${env.TAG_NAME}_${env.VERSION}"
        //                         sh "docker tag ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:build_${env.GIT_REPO_NAME}_${env.TAG_NAME}_${env.VERSION} ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:${env.GIT_REPO_NAME}_${env.TAG_NAME}_${env.VERSION}"
        //                         sh "docker push ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:${env.GIT_REPO_NAME}_${env.TAG_NAME}_${env.VERSION}"
        //                     }

        //                     //Enviar Notificacion a Slack
        //                     slackSend(channel: "#${env.SLACK_CHANNEL}", color:"warning", message: "PIPELINE - DEPLOYING -> ${env.GIT_REPO_NAME} -> ${env.EKS_CLUSTER_PP} - VERSION: ${env.TAG_NAME}_${env.VERSION}" )
        //                     slackSend(channel: "#${env.SLACK_CHANNEL}", color:"good", message: "PIPELINE - WAITING FOR SPINNAKER..." )
        //                 }
        //             }
        //         }

        //         stage('Deploy to Production') {
        //             when { expression { env.BRANCH_NAME ==~ /(master)/ }}
        //             steps{
        //                 script{

        //                     echo "Deploying version... ${env.GIT_REPO_NAME}_v${env.VERSION}"

        //                     docker.withRegistry( "https://${env.REGISTRY_URL}", 'ecr:us-east-1:aws-credential-registry') {
        //                         sh "docker pull ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:build_${env.GIT_REPO_NAME}_${env.TAG_NAME}_${env.VERSION}"
        //                         sh "docker tag ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:build_${env.GIT_REPO_NAME}_${env.TAG_NAME}_${env.VERSION} ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:${env.GIT_REPO_NAME}_v${env.VERSION}"
        //                         sh "docker push ${env.REGISTRY_URL}/${env.REGISTRY_NAME}:${env.GIT_REPO_NAME}_v${env.VERSION}"
        //                     }
        //                 }

        //                 //Enviar Notificacion a Slack
        //                 slackSend(channel: "#${env.SLACK_CHANNEL}", color:"warning", message: "PIPELINE - DEPLOYING -> ${env.GIT_REPO_NAME} -> ${env.EKS_CLUSTER_PROD} - VERSION: v${env.VERSION}" )
        //                 slackSend(channel: "#${env.SLACK_CHANNEL}", color:"good", message: "PIPELINE - WAITING FOR SPINNAKER..." )
        //             }
        //         }
        //     }
        // }  
    } 
    
    post { 
        always{
            cleanWs()
        }
        success { 
            httpRequest acceptType: 'APPLICATION_JSON', 
            contentType: 'APPLICATION_JSON',
            httpMode: 'POST', 
            requestBody: '{"PROJECT_NAME":"'+ env.PROJECT_NAME +'","BRANCH_NAME":"'+ env.BRANCH_NAME +'","CHANGE_ID":"'+ env.CHANGE_ID +'","CHANGE_TARGET":"'+ env.CHANGE_TARGET +'", "SHORT_COMMIT":"'+ env.SHORT_COMMIT +'", "DURATION":"'+ currentBuild.duration +'","STATUS":"SUCCESS" }',
            consoleLogResponseBody: true,
            url: 'https://devops-auditing.bancoripleylab.com/sd-auditing-cicd/v1.0.0/auditing/jenkins'
        }
        
        failure { 
            httpRequest acceptType: 'APPLICATION_JSON', 
            contentType: 'APPLICATION_JSON',
            httpMode: 'POST', 
            requestBody: '{"PROJECT_NAME":"'+ env.PROJECT_NAME +'","BRANCH_NAME":"'+ env.BRANCH_NAME +'","CHANGE_ID":"'+ env.CHANGE_ID +'","CHANGE_TARGET":"'+ env.CHANGE_TARGET +'", "SHORT_COMMIT":"'+ env.SHORT_COMMIT +'", "DURATION":"'+ currentBuild.duration +'","STATUS":"ERROR" }', 
            consoleLogResponseBody: true,
            url: 'https://devops-auditing.bancoripleylab.com/sd-auditing-cicd/v1.0.0/auditing/jenkins'
        }     
    }
}